Lots of brown, that\'s what I remember.

Or red, was it Red?

There\'s Red Rocks and the Red Rock Ten,

Names aren\'t really creative back home.

I miss those days, maybe not that place -

So much sand and dust and bone.

Why so many bones?

Not like death has lost his grace

In all the greener places too.

Here there\'s red and brown a ton, but up,

and orange yellow tangerine sky.

Things will run to holes and hide, as if afraid

to lose the green that\'s new to me.

I should be too, be loathe to part from all this life,

but I\'d rather remember there, so open - free.

No fall at home, no: nothing living in the desert to fall.

Nothing up but underneath,

No spring either,

Nothing born but all that\'s always been.

Wait, that can\'t be right\...

So much New here!

So much treasured there.

So much Green here!

So little home.
