Cries for you, these ever soft,\
They flow not through oratio\
But amble in extremities that loft\
Reactions to my inner wants.\
Not need - no, need is strong;\
Want is small and quiet blows.\
I hear and speak, but do not aim\
To hear myself o\'er sounds of same\
Of your still life, your fiery day,\
And overwhelming need to lay with\
Homogeneously sexual faces of you.\
That quickened, urgent spur imbue\
To you, after who? After passing through\
The longest list of days dead generations.\
How many meet to leave and meet anew?\
How can one still hear another life amid the din?\
I cannot hear myself, is this because my voice is thin?\
I could still shout to make the world but louder, blithely bludgeon
quiet,\
Not a smidge of secret silence anywhere if yet the you had time to pay a
painful pittance -\
My motions call\
To all who look to see,\
A weary wreath in all.\
And therein lies the glee:\
If only you who love can find me\
And I am never found,\
What am I to wonder but that\
I am heaven bound?
