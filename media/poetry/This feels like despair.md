This feels like despair\
This air is dead,\
But young.\
I can feel the anger\
I can feel the anger\
And understand\
But all I feel is shaken\
Uncertain\
No idea\
Hope but\
Fear is real\
They are supposed to be\
The ones all ruled by fear\
I\'m trying to not be afraid\
I never thought I was brave\
I still don\'t think I think\
I am going to live\
We need more\
Many more\
More to fight this\
We keep learning\
But knowledge doesn\'t spread\
Our country\'s dead\
But not just our country,\
The world is inconsistent\
Is there always hate?\
Is there a way\
To never deal with this again,\
To never feel this terrified\
Of people who are terrified\
Of people who are terrified\...\
There is a wrong, and\
We\'ve all found it.\
This is my first\
Burden to bear\
Pregnancy scare\
But not a scare\
We have to bear this one to term.\
This is how it feels\
To say that this is what we\'ve done.\
I\'ll be proud tomorrow, someday yet,\
But we begin with this regret.
