I left my doorstep in the morning, determined to find the hotdog stand
that had just yesterday impressed me so. The purveyor was full of tips
and tricks of the trade: this meat is perfect for tenderness, and these
spices will burn in the oil unless they're perfectly fresh. I had
believed every word of it, because what reason did he have to lie? It
was a wonderful hotdog. I trailed along the sidewalk, attempting to fit
one long stride inside of each short block of stone. I found in the
place of yesterday's stand a Vietnamese restaurant -- pho. It's the best
pho I've had since the last time I've been able to find somewhere that
served pho. I checked my phone, saw the invitations to Tom's, to
Sarah's, to Garen's concerts. I would have had time to see one, but I
likely would end up seeing all three because that kind of thing meant a
lot to people. I would want other people to see mine.

I saw my house coming up, the beautiful but bold architecture and color
scheme that showed the world just the type of person that lived there.
It had been at least three months after I designed it for the housing
machines before I saw one like it pop up, I think somewhere in New
Zealand. I still have the record among my architecture club, and it
works better in New Zealand anyways. The door creaked inward, something
I had wanted to fix at first but eventually decided was somewhat
charming -- it gave it a sense of reality in the imperfection. Not that
I'm the only one who appreciates imperfection. It's an imperfect world,
after all. How to fill two hours? I had a gallery opening with someone
--Jani-something? -- at four, and nothing to do until. TV, maybe, that
Netflix series just started that's supposed to be good. I've still got a
good three Netflix shows I've been wanting to watch, but that's all I
did last weekend, besides the party at Jani-something's.

Is there anything in the fridge? If not, I could get something pretty
quickly. I feel hungry, but not for anything sweet -- ooh, but they just
got my favorite tarts back in stock on Amazon... I don't think I could
really do anything as substantial as that stim-chicken-curry-thing, and
hummus just has too much fat. I checked my phone again; there were
hundreds of new posts to the writing forum. My last story still sat at
ten views -- not bad, considering. I tapped the video of the president's
minute spot on some late night talk show. Amusing, for sure, still don't
like her. I moved to my drafting table, looking at my latest finished
design. There was nowhere to put it, which is a shame because it's one
of my recent favorites. Whole building on one thin support, like the one
in Germany with a couple more towers. I tossed in fluted columns, just
to mess around, but now that I look at them... I erased the columns and
left a block-windowed façade in their place. Let capitalist society
shiver at my stinging accusation.

I sat. Many things on the wall, all from personal friends -- do they
think of me that way? Do I think of them that way, or is it just because
they're on my wall?

Namibia started a new movement yesterday, or at least the one from a
year ago had been declared dead and outdated. Shame, they had kind of a
Dali-Gogh-esque thing going. I'm personally back into Dadaism with a
romantic kind of spin -- real abstract stuff, but bold colors. I can't
get anyone to buy them, but people say they've got lots of merit. They
especially seem to like critiques, doesn't matter on what. I mean, I
really like their critiques too, so I can't really say much. I looked at
the clock, and sat back again. I looked at the clock again, but no force
of will would speed the time. I turned on the TV, and watched something
new for a while. It was good, really good. I looked at my phone, browsed
some pages, and again. Still an hour left. Too awake to sleep, and I
haven't done enough to warrant it anyways. I don't know what there is
left to do. I sit.
