The form of pain and rage we write - our words.

They fill the pages wall to wall and back

Again, to fall as food for lonesome birds.

That fire, that wills itself to break and crack

The prose that locks it deep inside; not our

Frustration that seeps from the page, not my

Passive-aggressive breaking-of-the-Rules

Whose pow'r and comfort always bring us

Back in time.

Only words on white that fade to grey.
