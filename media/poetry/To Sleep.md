Am I always tired?

I wake and go to sit

And sleep.

Have I always been tired?

I buy and buy caffeine -

The gold and black, Columbian;

As dependant as the sellers are

To my five dollars a day.

I\'m addicted, still, to sleep;

It takes up half my day,

And I think about it all the time.

Always dreaming of leaving

and falling back again, to sleep.

If I miss a fix, I cannot see,

Cannot focus, no longer me.

My mind rebels and tries to force

Or prod - convince me back to sleep,

Lets me forget to stay awake.

Blood shot eyes, that whitewashed face,

And headaches.

Why fight that glorious balm,

Why stand to keep from sinking?

\'Tis not to drown but rest, to sleep.

To rest and leave the world alone.

To sleep.

To sleep.
