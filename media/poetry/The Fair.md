All hail the tired parade,

Of ups and downs of every size!

They've got Facebook and twitter,

And hearts free and bitter,

But never, ever, do they stop.

Then come see the grand circus;

Grand and petite, fit all into yoga pants.

Walmart people, that's it -

and then the ones that do crossfit.

Modern wonders; today's newest dance.

Life's just a game

with little white pieces.

Well, the pieces have been white

For most of the time.

That bastard Johnson from work

Owns all but half of the spaces, and

The time that you pass his

Is the time you hit taxes.

You do not pass go,

There's no way to win;

Just hang on and come on

Along for the ride.
