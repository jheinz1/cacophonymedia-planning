The Jig of the Oil Rig As workin' along the earstern coast Were I and me
lads there bound, Findin' a fortune black and deep A hundred dark miles
down.

Heave and ho, and Let 'er blow! We'll know that we hit it big, And we'll
a be sing'n to the fire The jig of the Oil Rig.

We sailed a' right on in to town Oer six months lost to sea, And I saw
her purple ev'n gown A callin' right to me!

Heave and ho, and Let 'er blow! We'll know that we hit it big, And we'll
a be sing'n to the fire The jig of the Oil Rig.

I sidled up along her side And said my charming quip, But to my complete
surprise she said, "Sir, you're quite a massive"...ly rude gentleman!

All together men!

Heave and ho, and Let 'er blow! We'll know that we hit it big, And we'll
a be sing'n to the fire The jig of the Oil Rig.

Heave and ho, and Let 'er blow! We'll know that we hit it big, And we'll
a be sing'n to the fire The jig of the Oil Rig.
