Once, there was a man named Ted. Ted looked out his window as he had
many times before. He had seen many things out that window; hope and
love - sadness and despair. It seemed to him the most beautiful thing.
It was life the way it was meant to be lived - full of good and bad in
equal measure. He felt that he understood life quite well, what with all
of the time he spent observing it. It seemed quite simple, really, and
sometimes he could not, for the life of him, understand why other people
had such a hard time figuring it out.

Today, his window showed him Little Bill and Little Annie; they were two
of his favorites to see because they were utterly adorable. Little more
than six or seven were they, though Little Annie was a little older, and
they walked and talked the day away. Sometimes they fought, about little
things like everyone else, but they always forgot why in the end. Today
they just swung in little swings on the swing's little seats sitting
there 'till the sun had almost set.

'Well they must go off to bed sometime soon,' thought Ted. 'They seem so
very tired, and it gets so very late.'

He looked into the window again and saw Old Ellen. Now this he had seen
before; in fact it was the oldest story in the book. There she sat, in
her dusty old rocking chair, reading crinkly old books, and waiting,
deep down inside somewhere, for her cranky old husband to come home. It
seemed so sad, to wait so long in an old chair with some old books.

'How can she sit there for so long?' Ted wondered. 'She must get tired
of being so sad.'

Ted looked into his window again, and this time he saw the thing he
hated seeing. The thing he looked away from whenever he did. It was Ned,
and Ted hated that Ned rhymed with Ted, and so he dreaded the very sight
of Ned. He could not stand that Ned never ran, never showered, and
never, *ever*, read. But most of all, he hated when Ned got angry, and
boy did Ned get *angry*. He would puff up and shout, and get all red; he
was mean, and bitter, and hard on people - telling them what they needed
to do better all of the time. Worst of all, he never tried making
himself better, instead. And Ned went to bed, the only one in his own
head.

"Why does he pretend like he knows everything?!" exclaimed Ted. "Can't
he see that everyone else knows more than him?"

Ted grew tired of watching Ned and got up from his chair to get a little
snack. When he returned, he changed the channel back to Little Bill and
Little Annie, who were tucked in and fast asleep with little to wake
them. That made Ted feel quite a little bit better again.

**THE END.**
