Parylized

Shared shuttered eyes

Bolted windows - doors

See outer light,

On branch alit

Skin feels sharp

Soft pane of glass.

Lay on cornered carpet

Floors so full of dirt and easy,

Effortless class, I cannot leave or stay.

Sapped and then again so trapped

That I sit in sure attention rapt -

Flashing white and information

On the state of dying nation.

Oh the wonders idly waiting;

A weary world that bears berating.

Auntie, Uncle, I love a scandal

Amongst the data - too much to handle

For younger minds with little stored

Let alone the past.

And fill and file away, all day,

The new in-fasion way to pray.

And yet, the mass must stay up-date!

These angry eyes, entranced in all the

\"Fuck the police\", \"Patriarchy\".

Too serious for my \"Malarkey\",

For they know better, than each other -

This wounded world is too much fun.

\"Down with capitalism\" as they sign

A ten year, tenured, lease.

To teach dissent and rob the young, it\'s

Like a days-past dead man walking -

Laughing - somehow living; hung.

A billion conforming non-conformists,

Reading twenty thousand more BuzzFeed lists

On how bad other strangers are.

A herd of sheeple-watching people;

The fucking gall to still deride a steeple.

You cannot be alone the clear-eyed,

When fasions are to know it all.

When everyone can be, unique, the cynic -

Themselves immune to any fall.

There are no poets born from reading only poetry;

How can there ever be when

Mind has itself no time to breathe,

To flee from fine and free its fire.

A hundred minds have read aloud

The same sad stale regurgitations.

And they, the first to trumpet proud

Some singularity into the microphone.

How can words be grown to bloom

When wide the world is blown inside

By stiff\'ning wind until the womb,

The soul must in itself confide

That there is no room for tending.

Fuck this stiffness in my bones and

Fuck my overflowing tomes, for

They allow me e\'er to flee myself.

I long to free myself from couches

And from how my body always slouches.

Learning, that\'s the name that sugar coats

The most alluring of adverse addictions.

My game of choice, that made

A gambling man of me.

Knowledge for a salary,

Salary for celery,

And mirepoix for a stew.

I wish I fucking knew what tiny piece,

Of all my work and reddened tears,

That I can say my silken efforts grew.
