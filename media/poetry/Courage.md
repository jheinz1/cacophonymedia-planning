It is truly remarkable,

this life.

We live and die,

And ask it why,

But live despite the silence.
