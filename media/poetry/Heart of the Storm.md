I walked away through the clear winds alone, and fell

Far flakes of whiteness surrounding the throne.

This wasteland was mine when the world would never wake, I

Ran through the freezing its beauty to take.

A moment, no longer, that a jewel was shown to me -

Never but when I had reached out to be

A swirl in the starlight, no more and no less, to

Be nothing, have nothing left to confess.

Still walking and wondering what can warm a heart up,

Begging you to rest your feet and undress.

Weak willed and wanderers will never start up

Again and again they endlessly digress.

Cold grips my hands now and shakes them with vigor -

The winter she knows that no peace can there be.

Ever in torment, her beauty and anger,

And chilled heart are all iced in crystal for me.

Standing too long there transfixed by its grandeur I

Know I\'ve committed some barren mistake -

No pleasantness comes free, no kindness demure but

It bites in the end - uses diamond to rake.

Feeling it sink now inside my soft clothing,

I hear the blue sharpness meeting my bone.

Saw It but a second, all sterling and glowing,

And now I will die, oh so happy, alone.
