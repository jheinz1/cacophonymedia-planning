What is a mask?

A paper face?

No one really hides through every day.

I can show myself and be accepted.

Why would someone

That I talk to

Talk to others less without me there,

If *I* can show myself and be accepted?

There is a kid who's

ADD here.

We all talked about the way he talks

Too much.

But *I* can show myself and be accepted.

"I lost the will to

Keep pretending.

I could not have shown the failure that

I am."

She could have shown herself and been accepted.

She should have shown herself and been accepted.

I'm older now, can this

Be more than juvenile?

I've never known someone

That glows the way she does.

Oh, to tell her that...

Why does that one thought give me such fear?

I can show myself and be accepted;

I can show myself and be accepted.

Just one page,

A simple purposeful essay

That holds a thousand days ahead.

No big deal,

A single page.

Can I show myself and be accepted?

I don't belong.

But I hear that they all say

The same three words

In every job,

Every office,

A full generation.

Who can show themselves and be accepted?

But all we do is show,

Exhibit,

Perform,

Accept.

I begin to think

I *am* accepted,

At least outside.

I can show myself and be accepted.

But in my mind,

New frontier,

With novel time for exploration,

All is not kind.

Voices viciously eminate

From paper people

Imported from experience.

I can show myself outside and be accepted.
