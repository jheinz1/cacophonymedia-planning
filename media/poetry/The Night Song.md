The night song creeps through careless cabin carpet-linings,
a simple whispered word of wary hairless comfort.
The night song cowers comfortably,
Deep and dank in cowls of starlit silver shimmering.
Sweet and still refrain that sinks in spirit at the surface of the river deep
And dank in caverns unseen and unimaginable.
Deep and down below the line of level breath,
of rationale and wakeful thoughts,
it seeps.

The night song grows in tic-tock-time revolving,
Sweetened sickness perverse as a punchbowl poison.
Howl, and break in stride, as tickles catch the nape of neck.
Shivers sounding clear as cackling knee bone barrows
Carry by the first unlit snowflake of the long dead season,
Cold as to invite such shivering.
Twirling gently, icy cutting thing, serene pinprick of absent warmth.
Howl again, to mourn the silence, as frozen whispers pierce the heart.

The night song sings a silken serenade,
Stilted by sandaled stuffy sambas in houses of bubbled fire.
Hot and heavy ham-bone harassment houses,
Hell-bound and bravely bent on bomboleo;
Passionate floral flamencos bom-bom-beating out fevered favors,
Full of sweat and skin, springing smiling spirals from careful control.
Blackness bears down with cascading consequence,
And fires fail, flicker, fade into a field of glowing sparks.

The night song moans in married memory;
Collected cares of absent minds all fall to fill a silver bowl.
The endless robe spun black as sorrow's swaddling clothes
Drapes calm across each vine-like vericose vein-
The pulsing parade continued persistant,
Though small and muted in minutest palates.
Peaceful plosives, instinctive insect instruments employed
In the brimming emptiness of nature's nocturne noises.

The night song soars with wings of sky,
Concatenated needle-works of conquering constellations.
The dome above adorned with dotted dandelions,
The Dominus Dei,
The ceiling of the shadowed world and passage to the planets.
Furthest depths reach down to the only eyes and open;
The last awake alone to witness the endlessness of universe,
and life, and beauty.

The night song revels.
The night song recedes.

The night song returns.
