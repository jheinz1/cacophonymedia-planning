I tripped a time ago to Köln\
And swore that there I broke a bone.\
Fickle fantasy there fled\
To far off land and daisies bred\
In wondrous place,\
And yet in space!\
I want to go so far away\
To dance mein Herz und play\
A fitful jig of lunes,\
A hand of light absurdist tunes,\
As only briefly will I float.\
I\'m but today a flightful boat,\
To dare the seas and crest the wave,\
To roll and sing a joyous stave,\
To slip and sink and tumble on\
Though may I morning yet be gone.\
For a beat reels Meine Seele\
And swings my life as on a wheel, a\
Grand old soaring seaward spoke,\
Upon the glee of all to poke.\
I cannot now account for this,\
Der schwindlig Herr dass glücklich ist!\
Here, yet alive, and salty springs\
Through troubled waves, und sauber singst\
Zu mir - bevor du, traurig, schriest West.\
Ich träume dass du zurück bliebest!\
What laughing levity can pray, then,\
Dass lässt mich jetzt dahin zugehen?\
I dance, I fall, I break my tongue,\
As all the ancient songs are sung.\
Du siehst mich hier, keine Wörterkleider,\
Meine Rüstung ja - stärker, leider.\
Who stood before in solid dreams\
From which a mite opaque now gleams?\
That zieht mich bald zurück from light\
And fallen fancied flexing flight.\
Ich liebe dich, will I remember?\
When I sit still through long December,\
White December, cold and snow\
And wind that stiffly bodies blow.\
Return to warm, hab\' dich verpasst!\
A minute more, so sunshine last!\
I feel mein Herz und Seele voll!\
As if I\'ve passion, purpose stole,\
From plastered strength and stony mirth,\
To give myself a spotless birth.\
Und du bist hier, immer mit mir,\
And never will ich fertig fear.\
Du hast mein Herz, du bist mein Leben,\
And I have alle mich nun geben.\
Silken arms and silver tongues now\
Dab the soothing milk on sunned brow.\
Die Lerchen singen leise über mich,\
Und pflegt mich ein klein müdes warmes Licht.\
I feel my head about to sink\
And crown the plane above to think.\
Ein moment bis, mein letzt Adieu,\
I will, someday, return to you.
