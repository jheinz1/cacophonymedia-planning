So here we are again,

Me and you,

At this same old tea shop

Half past four.

It seems like yesterday,

How are your kids, and

Everybody's doing well.

I really am happy for you,

And things are going great for me, too.

So I tend to like and not like,

Though I lack in love as much as hate.

I smile and frown,

But neither tears of joy,

Nor sorrow,

Dance and falter down my cheeks,

Lighting on my lips

In laughing lines on brined and burning skin.

I am a tasteless animal,

Not one to be eaten,

To be enjoyed, nor to enjoy itself

Free in the open until Death's love

Of fear and prudence is sufficiently offended.

I am that that stays alive,

And has no earthly reason not to be,

While others are chosen to die,

Or to be let to Live.

While others are salted and served,

While others are enjoyed even at their own expense,

I am alive.

You know,

It is not entirely that great.

That this is what it is

To be normal.

This is what it means to be okay,

To be able to say

That nothing's wrong.

I just want to feel

Something

Today.

Though, I suppose,

That is no good subject for afternoon tea.
