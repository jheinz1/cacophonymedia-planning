One by one I once met a man named Samuel; He went around the world and
met lots of girls. But in the small town where he came from, He started
a little chubby and thought he was dumb.

He read lots of books and wanted to like 'em, But then he got older and
tried to get thin He took to the ladies more than the books, And learned
how to work them with charm and good looks.

About the same time in that podunk town, Lived another man I know named
Jackson Brown. He was kinda chubby in mid-school, too, And read lots of
books with nothin' else to do.

He loved the books, and got real smart. He slimmed down, but kept to
school to 'get a good start'. He left the ladies wherever they were, And
studied school instead of studying 'her'.

Now she was what he wanted to learn, But he let her decide he didn't
deserve a turn. Jackson didn't know how to make her tic, But one of his
friends knew, and could do it real slick.

Samuel met her on their high school senior trip. Jackson watched as he
watched her lick her lips. She snagged him a bit like she had Jack, And
he snagged her a little bit back.

But in a few words dear Jackson saw That somehow she had strung both of
them along. 'Nothing can happen, It's totally safe.' And Sam's fine
smile was left in her wake.

Two little heavy kids two towns apart, Oh so alike at the very very
start, Came back from their paths to meet at last, And, one by one, left
her in the past.
