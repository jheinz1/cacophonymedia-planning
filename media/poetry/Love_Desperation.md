I don't know why no one will take you - though I do not wish to be
parted, but to lure in and trap another to join us in this dark room we
share. I love you, but I cannot remain with you. Alone with another
alone.

Is it mine to apologize, for sending you away to save us? All I've read
has said that some passing bus, or ship or raft at least, will come and
take pity on the stranded.

I have read the wrong books.

There are more blind in the sea than I could have ever have known; they
have teeth and claws, and they do not see but wound as they pass by too
close. They leave blood in their wake. I will not apologize for the pain
of the world, but I will hold you now to my chest and cry. Tears heal
just enough; not the deepest, but they buff the surface to its necessary
shine.

Can I apologize for that? The unending standard of happiness, that we
can never look to be hurt because holes are unsightly? But how should
blindness see weakness? I've heard that silence feels a strong arm, for
better or worse, but we can be the better! I don't know why I must force
you to be pristine. I never wanted to be my father, my early father, who
'knew me better than I did'. Or the father from the books I don't read
who loves nothing less than perfection. I only want to protect you; I
have so few ways but to make you stronger.

You don't need to be impenetrable, but if you can be then none of them
can hurt you.

I am sorry that I have disciplined you for admitting you do not want to
be lonely,

Of the many things that I cannot admit to. I am sorry that I ask you to
love the hand that denies you; I am sorry that no one will love you if
you ask. I am sorry you must be kind, for kindness is gripping tight a
rose so another may smell the petals.

But what would you do? I've been told to listen, so what is your plan?

I'll tell you the rules:

We cannot hurt.

We cannot care without pain.

We cannot live without caring.

I have done my best, but I can't make the world better. Someone will
hold you, that I KNOW. I cannot speed that day, and all I can think to
do is speed you through all of the beating and the being bruised so that
the someone who will love you like I do may reach out to save you. We
have lost the game of probability again, but we can't be losers every
time.

I can make home healing, but I cannot warm the wastelands. And I love
you, so very much,

but I cannot go in your place. God, I wish I could. Forgive me, and
please, go.
