Bits and pieces,

Ash and dust,

Swirling round or

Gath'ring rust,

Isn't it a wond'rous thing --

This stuff.

Whatever may make up

All our lies

(Whereever a soul

May actually cry),

Really awful broad a meaning

In the end.

But just a bit,

A little piece

Of yours would I

But die to lease --

And live there 'till forever comes

And takes it all away.
