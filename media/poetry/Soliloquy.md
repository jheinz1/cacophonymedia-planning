And I find it not a flight of fancy

So much as fate that found me here.

Late lost generations home at sea

Asleep on storied sorrows and buried fear.

Legends of the lesser lives of love,

The quiet places picked from power

Are too much to think on, God above.

To have the chance to choose to stay,

A foolish dream for all but recent days.

And yet the pressure weighs now more,

For gone are all that force the fall.

So let me frame my love the whore

And on my heroine heap it all;

These fears and doubts and angered speech

That days of old would see to pompous fools

Have no more resting place, cannot impeach

Nor fire against some unjust villainy.

The way is clear and none but fools look back,

But now the path is lit the stones are black.

May I yet dream of some unkind machination?

Some knave of court that just days hence

Were to steal you for his torrid fascination?

And what ill temper is mine of all the gents

To wish for us a wish of no tomorrow? But,

Does not a knight require hay to house his lance?

Should lancing wit, my chosen arm, now borrow

Marks of those I love, or else be laid to rust?

Of all the graces heaven brings you gave

The first and greatest gift of empathy,

And all the wearied world this star should save.

But lost along are loves forbade and devilry.

The count to slay, the purest passion born,

Are laid to rest for safer, brighter storms.
