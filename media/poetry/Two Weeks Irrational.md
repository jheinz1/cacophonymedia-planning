My heart aches for you, my dear,\
In ways that green days in early years\
And evening bedtime fears supposed\
Existed naught but amidst my Shakespeare.\
\
I dream of lazy moonlit piers,\
Of poking fun at southern songs\
That cannot help but rhyme with \'beers\',\
And, in breaking from my rearing,\
Dream I of heat.\
\
Oh subtle Sunday someday nipple-piercing,\
Oh lace beneath still-softer sweater.\
Of soft on smooth, your dermal mem\'ry breaks\
My meditative concentration on your absence.\
\
From heat to wamth now, arousal fades to dense\
Impenetrable missing-of-you, to riding the fence\
Of drinking the dew or resting in morning.\
And sorrow then compels my aching bones\
to remove me hence.\
\
The son that shines! Or thought of children;\
Quiet bark in the wood! Or shall I break for want of fur?\
A whisper of wind, your lips at my ear;\
A stone in the river,\
Heavy head on my chest.\
Silent, stilted, quiver.\
Nothing is near;\
nothing - Nothing - to fear.
