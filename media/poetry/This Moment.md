How to begin?\
This moment out of\
time,\
That my addled mind would so quickly shirk\
If only to encase in rhyme,\
Belongs to me now and is not mine.\
To hold is fleeting, the least little act\
That hands can use to keep another still.\
And yet, to hold is two - a union -\
That, though weak, can bind and tie.\
This holding, fear and acceptance of release.\
The horizon, gilded intrusion, will always come,\
And every iron-clad connection scatter.\
Though some hold out as if an everlasting longing lifetime!\
I do not know -\
Know so little of the world -\
You may live past these protean words,\
And yet I rest knowing that they live.\
I have never held you before,\
I have never held before you.\
Never been a shoulder, a pillar,\
More than happy conversation filler.\
I feel this, your body, self, and sigh\
As mindful pieces scream and vye\
For extra moments or more to say\
Until the nearing break of every day.\
To write or soak, to hold in space or\
time?\
I cannot reap, nor dare to let it lay -\
This new and ripened fruit sublime.\
And what can stasis bring but early ends?
