Now I have lived so let me tell

Of all of the things that I have seen

Fair freshened fields so garishly green

Gastly in their gall to tell them

All Dead fields of gilded gem

A tall tale of life

I saw that once I did

Now I have lived so let me tell

Of all the things that I have dreamed

Of dancing driads dallying citrus screamed

Bright sour Hear laughter damp with daisies

and dry dour wit

I wish that I could remember it

Demure sheets my emberbarrassment hid

Now I have lived and so confide

Of all the things that duck and stride

Of shadows sulking attempting to bide

some softened skin to menaced hide

The ones that stalk Still sickened sallow

Sucking shallow breaths so sharp as ice

Sing so soulful of rerolling dice

Verve and fire still seldom slid

Now I have lived so let me tell

Of only what could give me life

The yesterday winter wonder walk to wife

Who woke waved and put all else to bed

The thought of being one day wed

But not to woman if more to world

For her is all I\'ve ever truly known

Who all I short lived undid
