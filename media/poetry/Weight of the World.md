Speechless in the face of talent,\
What a useless thing to be.\
To pale before the sun is normal,\
But see the stars\' continued light.\
Should you shrink down as another\
Grows their mental stature, other\
Brighter light is smothered - lost.\
We move and make, but only if\
Our hand can start another shaking.\
Should one disparage the face of\
Art that queries just what you\'re made of,\
Then all the world\'s a thief.\
Your future work it\'s inspiration\
Has shirked for mere self-deprication.\
The greats do rob you, you of then,\
But as do you unto the world -\
The same as gift of fired men\
On walnut wooded frame.\
So these great works remain just so -\
Unchallenged.
