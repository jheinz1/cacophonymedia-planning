The world has run through its pathway,

And all that is left is the space inbetween.

Ever fear, for here you die.

Ask why we lie, awake, dishonest,

And there it is.

That dark black chasm,

the one that\'s all the unknown,

that we cannot see because

we will not see -

just turn a step, a rock, you leapt

fallen down as many others.

Inky blackness, stolid, crept,

and swallowed sisters-mothers-brothers.

One may not cross the current

unless deterrant wall or bridge does span it.

Still we step in with longing heart

that is swept away softly into dust.

A greyish box, that\'s home sweet home.

It\'s quiet, this, a solemn throne.

Alone, away, and sealed quite in,

with no solid purchase yet to win

from blackened walls without a seam

as a silken white and blackened ream

or crown, if pretend is more your type.

I sleep here yet by endless night,

For I, as you, I cannot look

to right

to left

as chasm\'s go, this takes the cake -

though I am one of infinite islands

stranded in its icy sea.

Spend your day on bridges,

It\'s what I\'ve chosen to do.

Convey yourself to woo me so we may both

reside outside of solemn prayer.

More than silent tears and screams

that never make it past my bedroom walls.

I want to hear you scream, let you scream,

Let you cry, cry with you.

But what use is it for me to send my tears

In wishes to your off-direction?

How many bottles wash upon the shore

when cameras aren\'t there to see them?

Bring a bridge to me,

as I have learned my craft and started

reaching out to you.

Cross the horizon-lacking sea

and we may soon be long departed.

And learning that, though bridges met us,

we may fly to married, empty, open days.
