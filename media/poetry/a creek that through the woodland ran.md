As I hunkered in the brush I saw

a creek that through the woodland ran.

It - or he, it seemed more then,

made me think on where his course began.

His summer song calls out among the trees

and wary rustles answer back;

The wind wakes them now to watch and see

what exuberance they lack.

He leaps and rolls o\'er stone and stick and sand,

and shakes off winter\'s sleep with every turn;

He knows he runs and laughs through Mother\'s land,

who\'s ways he can\'t but hope to one day learn!

The trees look on and hide their distant minds,

their new and tender buds watch daily o\'er,

but strangers in the forest only see

that lone and wondrous one run ever more.

As I hunkered in the brush I saw

a Creek that through the woodland ran.

It - or he, it seemed more then,

swept my thoughts to where his course began.
