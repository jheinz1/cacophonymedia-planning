Oh

What

Sweet misery

Is silence.

Louder can it ever be

Than whence

The torrent came,

And crashed and swelled,

Devoured boats and ships and men;

Receding then

From whence it

Came.

But silence

Only comes in true

To follow that which means

The most.

To follow pain,

And war and need;

The sobbing ones -- with sunless lives

And lifeless sons --

Does silence like to seek.

Is it only there,

In still

Calm

Waters,

That we

Can truly ever

Feel peace?

Cry out,

Ye mothers,

Ye martyrs, ye brothers,

But do not expect

To hear

A
