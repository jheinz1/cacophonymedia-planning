A day among the rest

When I am dressed my best

One twitching eye closes

Only for a moment.

Open and open my mind,

Ready to go and find

The path among the wreckage --

The dust that falls from age to age.

It's not my dust,

As it falls as rust

In trails from lock to leg

I find myself on knees.

I wiggle toes,

My mouth, wrings in throes,

And I walk again through some new horizon.
