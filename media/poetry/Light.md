He lifted up his heavy head

From night\'s ever blackened bed;

He brought bright blistering light,

And thus drove the dying night.

Light he gave and gave again-

Woke again a million men.

The sun in youth and golden life,

Cut eve and dale with golden knife.

At noon he ruled o\'er men,

Every mountain, hill, and glen.

His throne was high in every sky,

And earth did his banner proudly fly.

Shadow hid in light of day,

Giving light its iron way.

\"Patience, patience, for some time soon,

He will lose the strength of noon.\"

The light then grew slowly old,

And slowly lost his iron hold.

Gone were daring, strong and brash

As he donned wisdom\'s shining sash;

Loved more now even than before,

Light neared his final shore

As shadow crept through every crack

And longed to have its kingdom back.

Then the golden light began to die

And his flaming pyre lit the sky.

Red and gold painted the air,

His final breath sent everywhere.

At long last it could then return,

Shadow\'s own light finally burn.

Stars shone like crystals clear;

The reign of night at last was here.

Mourned then was beloved light,

And shadow named a cursed blight.

Only few saw the blessed sleep

And quiet beauty that shadows keep,

For thieves and silent demons knew

That darkness was their golden hue.

Thus endured mute shadow\'s reign

And, in turn, was age its bane.
