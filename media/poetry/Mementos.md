[I\'ve rifled through all of it -]{.mark}

[I promise you, time and again.]{.mark}

[Packages placed all precariously up,]{.mark}

[Lying this way and that way in]{.mark}

[\*Every\* corner of my den.]{.mark}

[I found those old Paris playing cards,]{.mark}

[The ones missing all the twos.]{.mark}

[And your encyclopedias, wrapped in Burnt Sienna,]{.mark}

[Each of them half dust and half paper.]{.mark}

[I\'ve read lots of old books,]{.mark}

[Removed still pots from their nooks -]{.mark}

[Saved this and that odd thing for later.]{.mark}

[I\'ve been through everything now,]{.mark}

[Yet in none can I find it -]{.mark}

[In little boxes, bigger boxes,]{.mark}

[Bitter brown old digging boxes,]{.mark}

[They may as well be empty boxes]{.mark}

[All scattered on the floor.]{.mark}

[The walnut floor, so softly orange lit,]{.mark}

[Feels like a stone beneath me.]{.mark}

[It is now that I realize that I\'ve been]{.mark}

[\*Sitting\* here, for hours, with tears in my eyes,]{.mark}

[And for what?]{.mark}

[A tiny trinket of you among all of the rest?]{.mark}

[Because I have been blessed with so many!]{.mark}

[Every memory, mountain and meadow,]{.mark}

[And watching three beautiful lives grow -]{.mark}

[These boxes are filled with little but those!]{.mark}

[And yet,]{.mark}

[Since That Day,]{.mark}

[Since the last time I lived,]{.mark}

[I can\'t bear to look at these smiles.]{.mark}

[They were happy, oh these unknowing pictures,]{.mark}

[And I cannot part with a single one.]{.mark}

[These capsules - of miles we traveled]{.mark}

[And mysteries unraveled,]{.mark}

[Are too precious for me to ever let go.]{.mark}

[To lose but one is to burn them all -]{.mark}

[And where did that \*damn\* thing fall?]{.mark}

[Into tears behind my beaten bedpost]{.mark}

[To be forever lost to this world?]{.mark}
