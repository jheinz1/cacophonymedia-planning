The walls of Slane I lived for four long years in the shadow of the
walls of Slane. My time was spent in wondering why I even came,
Wondering how they got so strong and yet still stand cold, They laugh as
I try to break their icy hold.

Who and why had made them so I couldn't help but need to know.

In the shadow of the walls of Slane, Why must I feel so damn small? Who
built them to mock me even as I Long to make them fall?

I've left the walls so far behind me now as I write That I wonder if
they still could stand as strong. Or did they fall on some dark and
monumentous night To strength where my own love had always failed.

The gates of Slane will open again, And be some brute's savage den. But
there will be a better home for me.

In the shadow of the walls of Slane, Why must I feel so damn small? Who
built them to mock me even as I Long to make them fall?
