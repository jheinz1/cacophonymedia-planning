I want to see this with you.

Not to show you, or\
To paint, prosaically plaster up\
The reds and greens and all\
But everything that I can feel\
As I now sit here head to heel

In the closest low of light

And here I begin again in\
Failing, flow\'ry words all falling thin.\
I do not,\
Cannot continue to tell you\
Or to save these things away\
For when you return someday.\
Why return?\
As if I had you before\
In days that are only red and gold\
In backward angles ages old.\
Could I then have been complete?\
Have it, had it, but lost it somewhere,\
The it that can be only it and nothing\
But to vanish as soon as ever else.\
I want to feel this with you\
With you HERE\
Though I don\'t think I AM HERE,\
Not really.\
Do I fear myself,\
The present place and time\
And absence bless\'d, lone, sublime?\
That there is never more\
To here than me, the whore\
Of time and thought,\
And what stings the more,\
Of love and silence.\
I\'ve pushed you out,\
while you\'ve been gone.\
The heavy days of long\
Pauses, nothing e\'er to find\
But what you left.\
I cannot see your face\
Or hear your laugh,\
Nor remembers my mind -\
Dusty in a verdant-gilded-box -\
If you could laugh at all.\
A POX on that shabby attic\
that holds naught but the roof.\
I do not know if you were\
fair and kind, or strong.\
I wonder if I will ever meet you\
In silent, dusk-webbed truth,\
And yet you float throughout me.\
Clearer than a day ago,\
Or less to see.\
It\'s the smudges that make you out.\
And now, at night, a sudden bout\
Of clouded sight!\
You are nothing\
But all the world you make me feel\
And left me to walk without.
