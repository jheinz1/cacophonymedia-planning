# Brief Project Descriptions

# [Project Colossus](colossus/index.md)

Massive online PvE battles against titanic foes. Army vs Bahamut/Godzilla/etc. Maximum number of soldiers, defeat the boss or be defeated while using whatever you can find on the battlefield. OR, stop the monster before it destroys a city. Clear goals for how to take it down, but enough variables to make it different every time. Opportunities for cooperation (I.e. building and operating larger structures and weapons together) but no requirement for constant communication.

> Points of Reference
>
> Battle Royales/Extraction games (scavenging and building), Shadow of the Colossus, God of War, Zelda BotW, Final Fantasy boss fights, Monster Hunter, Dark Souls, Pacific Rim, Godzilla

# [Project Cumulus](cumulus/index.md)

Large-scale fantasy pvp warfare with vehicles(/mounts). Star Wars Battlefront meets Pandora/LotR. I want to make THE dragon rider game, though having everyone on dragons all the time pidgeon-holes it a bit. More like Battlefront/titanfall, where aerial/mech combat are central pieces of the game but aren’t the whole enchilada. Dragon and rider are separate, but strongest together. This is why Avatar is such a great example: Navi have a special bond with a flying creature and that is the big climactic part of becoming a warrior, but they still fight on foot, horseback, from helicopters, etc.

> Points of Reference: 
> 
>Battlefield/Battlefront, Avatar, WoW, LotR, Eragon, scalebound, for honor, titanfall

# [Project Illiad](iliad/index.md)

Possession mechanic inspired by sports games - all units have default operation, and you choose who to control. You can do supercharged things, but the other neglected “positions” can be taken advantage of by the enemy. RTS/shooter/MOBA/Zelda/etc. mechanics all rolled into one game