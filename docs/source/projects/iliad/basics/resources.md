# Tech vs. Magic

All units, abilities, buildings, and damage types fall into one of two categories: magic or tech. 

Tech covers everything of the physical, from machinery and computers to swords and fists. 

Magic is reserved for things that may have once been deemed impossible, including spells and the mental manipulation of objects from afar.

# Basic Resources

Three Main Types: Industrial, Arcane, and Elemental

## Industrial

This covers familiar mundane materials like steel and coal for creating buildings, machines, and weapons of direct conflict. Used for:
* Strength, defensibility, and efficiency
* Non-magical weapons
* Tech units and utilities and technomancy (magic that controls machinery - industrial magic)

## Arcane

This covers alchemical materials containing latent magical force. Used for:
* Flexibility, extensibility, and control
* Magical effects and damage
* Magical units and utilities and alchemy/infusion (tech that has magic effects - arcane tech)

## Elemental

This covers the motive components of the natural world, like magma and ice. Used for:
* Transformation, Balance, and Sustainability
* Weapons that serve multiple purposes (status effects, chain attacks, effects to structures, etc.)
* Magical and tech equally
