# Factions

Civilization-style: Mostly share the same basics with different skins, but each faction has a small set of unique units/boosts/buildings/etc.
Each has:
* A unique resource-related boost
* A unique combat/objective boost
* A unique mid-game titan
* A unique end-game titan

## The Stormforge Republic (Industry)

* Tanks and infantry
* Think humans, gnomes, goblins, dwarves

References: Starcraft/Warhammer marines, UNSC

## The Cambrian Academy (Arcane)

* Mages and robots
* Think humans, elves, gnomes
* Shield could be replaced by base levitation?

References: Piltover (Arcane), New Capenna (MtG), Exandria

## The Immortal Kingdom (Elemental)

* Benders/Elemental mages and spirits
* Think humans, elves, spirits, sprites, morons
* A society that is based on a deep connection to nature

References: Samurai, Avatar the Last Airbender, Ghibli movies

## Skymarch Incorporated (Commerce/Persuasion, Industry/Arcane)

* Steampunk pirates and thopters
* Think humans, dwarves, orcs

References: The East India Compony, pirates, Dune

## Gorgonia (Elemental/Industry)

* Beasts, Golems and druids
* Think anthros, humans, elves, orcs
* Lightweight and naturally-derived machinery

References: MtG Green decks, Gorgonites, Deserted island technology

## Zorcanus (Arcane/Elemental)

* Aquatic-inspired creatures specializing in alchemical combinations of magic and nature 
    * Elementals, Chemistry
* Think zora, 

References: Zora (Zelda), Alchemists, Witchers