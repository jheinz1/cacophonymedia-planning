# Project Iliad

:::{note}
Potential names:
- Hands of the Gods
- Divine Intervention
:::

:::{toctree}
:maxdepth: 2
:caption: Basics
Overview <basics/index>
Resources <basics/resources>
Factions <basics/factions>
Units <basics/units>
Buildings and Bases <basics/buildings>
Creation <basics/creation>
:::

:::{toctree}
:maxdepth: 2
:caption: Hero Classes
Overview <classes/index>
General <classes/general>
Champion <classes/champion>
Sorcerer <classes/sorcerer>
Engineer <classes/engineer>
Warden <classes/warden>
Operative <classes/operative>
Adventurer <classes/adventurer>
:::

## Development Planning

### Minimum Viable Product

Included Classes:
* General
* Champion
* Titan

Only faction: The Stormforge Republic

Objectives:
* Assassination of a General
* Annexing structures
* Lowering of defenses
* Theft of resources
* Mechanical Sabotage

Key Milestones:
* Solid RTS base (resources, upgrades, production, base system, etc.)
* Single linear map (blood gulch canyon style)
* Flexible system for switching between control schemes
* Vehicles
* Dumb unit AI
* Dumb General AI
* Fun-to-control titan units
* Base set of general-purpose weapons
* System for making power weapons available to Champions but at a limited capacity
* System for elevating (“Blessing”/“Empowering”?) base unist could be an up-front cost or an upgrade later on
    * If you have no “empowered” units you can still control the basic ones, you just have a limited set of abilities and no progression system

### Development Phase 2

Add Classes:
* Demigod
* Engineer
* Warden

Add Factions:
* The Cambrian Academy
* The Immortal Kingdom

Creation:
* Basic small structures
* Some examples of self-use tech and magic helper items

New Objectives:
* Objective defense
* Base set of random map hooks
* Multistage Rituals

Key Milestones:
* Basics of the tech and magic creation systems
* Sorcerer “boon”-style skill system (Materia?)
* How Sorcerer “boons” can be used by another unit that defeats the sorcerer (weapon improvements? Useful buffs, but weaker than the original skills?)
* Beginning of balancing for different classes/factions/playstyles

