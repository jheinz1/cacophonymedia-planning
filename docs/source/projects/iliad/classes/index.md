# Hero Classes

## General (Strategy and logistics)
* RTS-style battlefield control and resource management
* Good for general battlefield control and maintaining the flow of forces and supplies
* Skill Tree: Little to no direct interaction or skill tree, but a range of skill trees for units and production
* Creation: Sole ability to create new units and large structures
* References: Starcraft, Background units in MOBAs, Clash Royale/Bowmaster/Tower defense games

## Ranger/Marksman/Assassin/Spy-> Operative (Stealth infiltration)
* Assassin archetype with specialized tools
* Sniper, silenced weapons, one-hit-kills, parkour, etc.
* Weak in open engagements
* Class-specific interactivity with objectives (stealth, climbing, etc.) and disruption abilities like hacking and sabotage
* Skill Tree: Minimal, for class-specific interactions
* Creation: Only self-use tech helper items
* Excels in taking down high-value targets and performing tech sabotage
* References: Assassin’s Creed, Hitman, Dishonored, Cyberpunk

## Ranger/Wizard/Mage -> Sorcerer (Crowd-control)
* Third-person top-down battler with a range of passive and active abilities
* Good for open engagements and crowd-control
* No headshots and low precision, but can have critical hits
* Can struggle with single high-damage targets
* Very limited interactivity with objectives
* Skill Tree: Multifaceted, with lots of unique choices, and eventual battle-defining abilities
* Creation: None
* References: MOBAs, Hades, Top-down battlers

## Ranger/Knight/Warrior/Marksman/etc. -> Champion (Targeted strikes)
* First- or Third- person fighter that gets things done
* Good for executing key combat missions
* Excels in taking down high-value targets and getting past defenses
* Precise weaponry with high damage with one to a few targets, can pick up things from the battlefield
* Broad interactivity with objectives and vehicles, minus class-specific options
* Strong good against small numbers and with cover, but can easily get overwhelmed by substantial numbers of enemies
* Skill tree: None
* Creation: Only self-use tech helper items
* References: Master Chief, Doom Guy, Navy Seals, Achilles

## Ranger/Knight/Explorer -> Adventurer (Exploration)
* Third-person balanced unit focused on interacting with the world
* Good for executing key missions with only low to moderate enemy presence
* Excels in exploration, scouting, and uncovering things that can improve your army’s prospects
* Interactivity with almost all objectives, including limited crafting
* Excel at multistage rituals/discoveries with substantial impacts on the battlefield or your army (i.e. activating an ancient structure, summoning a dragon, etc.)
    * Required for discoveries that are dependent on key items
* Strong close-quarters options against individual targets, but easily overwhelmed
* Skill tree: None, but can use/upgrade key items: broad tools with a range of systemic uses
* Creation: Only self-use magic helper items
* References: Legend of Zelda, Jedi, Mario

## Explorer/Mechanic/Builder -> Engineer (Tech support/creation)
* First- or third-person mix of direct combat and crafting
* Good for battlefield control and indirect engagement
* Broad interactivity with objectives and vehicles, some hacking/sabotage, tech repairs and improvements, and class-specific improvements to key items
* Only enough combat tools to defend themselves well enough
* Skill Tree: Multifaceted, with branches tied to types of units/structures and abilities that synergize with General-level decisions for improving those units/structures
* Creation: Full access to tech creation, plus ability to build small structures and improve units and large structures
* References: Torbjorn from Overwatch, Fortnite, Factorio, Starfield

## Healer/Knight/Mage/etc. -> Warden (Magic support/creation)
* Third-person on-the-ground support - healing, buffs/debuffs, shielding/wards, etc.
* Good for countering active pushes from opponents
* Limited interactivity, mostly for mobility (no heavy machinery), but some crafting abilities and magical repairs and improvements
* Limited damage potential, but good defensive/ranged options and warding
* Skill tree: Yes, building to lower-investment abilities that are impactful but limited in scope (i.e. a shield eventually becomes impenetrable, but is still very localized, or giving a Demigod a free ability but can only do that once)
* Creation: Full access to magic creation
* References: Support classes in overwatch, WoW, and other class-based games, tools for defense in objective shooter games (rainbow six siege?)

## Titan (The big guns)
* Third-person high-damage unit with a fixed skillset
* Good at moving the needle all on their own
* High general defense, but vulnerable to skilled individuals (weak spots, boarding, etc.)
* Simple controls and few but powerful abilities
* Skill Tree: None
* Creation: None
* References: Tank, Gundam, Dragon, AC-130 in CoD, Halo Scarab