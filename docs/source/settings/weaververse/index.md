# The Weaververse

:::{toctree}
:maxdepth: 2
The Weaver and the Loom <weaver_and_loom>
The Great Winnowing <primordia/the_winnowing>
The Sowing of Finitum <finitum/the_sowing>
The Creation of the Gods <finitum/gods/creation>
Effi, God of Harmony <finitum/gods/effi>
Ilumeus, God of Knowledge <finitum/gods/ilumeus>
Morgrum, God of the Wild <finitum/gods/morgrum>
Noridonus, God of Culture <finitum/gods/noridonus>
Ordri, God of the Great Circle <finitum/gods/ordri>
Thonera, God of Creation <finitum/gods/thonera>
Nocrus, God of the Night <finitum/gods/nocrus>
The Unseen Arbiters <finitum/gods/the_arbiters>
:::