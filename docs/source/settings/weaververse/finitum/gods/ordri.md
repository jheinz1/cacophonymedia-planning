# Ordri, God of Time, Patterns, and the Great Circle

## Domains

- Mathematics and Logic
- The Sun and the Moon
- Truth and Wisdom
- Wonder and Attunement