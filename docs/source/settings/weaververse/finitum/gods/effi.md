# Effi, God of Change, Harmony, and the Sea

## Domains

- Love and Togetherness
- Unity and Bonds
- Evolution and Growth
- Flexibility and Patience