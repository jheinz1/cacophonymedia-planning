# Ilimeus, God of Curiosity, Ingenuity, and Knowledge

## Domains

- Education and Self-Improvement
- Scouting and Investigation
- Empathy and Understanding
- Comfort and Safety