# Noridonus, God of Culture, Community, and the Skies

# Domains

- Civilization and Commerce
- Civic Responsibility and the Social Contract
- Revelry and Luxury
- Ambition and Freedom