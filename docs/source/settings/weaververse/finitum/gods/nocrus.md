# Nocrus, God of Contentment, Death, and the Night

## Domains

- Selflessness and Perspective
- Loss and Grief
- Sacrifice and Acceptance
- Stillness and Peace