# The Unseen Arbiters

The Adversaries are spirits created to challenge and strengthen the people of Finitum. They embody the dark whims inherent in all mortal beings, and monitor the balance of those forces from age to age.

## Deceit

The Spirit of Deceit is the embodiment of subterfuge, poison, manipulation, and any other ill brought about by betrayal of trust or expectation. 

## Harm

The Spirit of Harm oversees direct conflict and all manner of torture, war, and pain. To many, Harm is the most familiar and straightforward of the Arbiters.

## Attrition

The Spirit of Attrition represents the indirect weakening of a foe through isolation or starvation, be it as large as the siege of cities and the burning of fields or as small as a cutting word or the witholding of affection.