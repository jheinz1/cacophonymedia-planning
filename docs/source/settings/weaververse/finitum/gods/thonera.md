# Thonera, God of Creation, Exploration, and Industry

## Domains
- Expansion and Cultivation
- Technology and Progress
- Efficiency, Skill, and Precision
- Control and Confidence