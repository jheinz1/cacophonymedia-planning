# CacophonyMedia Properties

:::{toctree}
:hidden:
:maxdepth: 2
:caption: Settings
Brief Descriptions <settings/index>
The Weaververse <settings/weaververse/index>
The Alterverse <settings/alterverse/index>
The Outerverse <settings/outerverse/index>
:::

:::{toctree}
:hidden:
:maxdepth: 2
:caption: Projects
Brief Descriptions <projects/index>
Project Iliad <projects/iliad/index>
Project Colossus <projects/colossus/index>
Project Cumulus <projects/cumulus/index>
:::